from ximilar.client import RestClient
from ximilar.client.constants import *

GRADING_ENDPOINT = "card-grader/v2/grade"


class CardGradingClient(RestClient):
    def __init__(self, token, endpoint=ENDPOINT, resource_name="card-grader"):
        super().__init__(token=token, endpoint=endpoint, resource_name=resource_name)
        self.PREDICT_ENDPOINT = GRADING_ENDPOINT

    def construct_data(self, records=[]):
        if len(records) == 0:
            raise Exception("Please specify at least one record in detect method!")
        data = {RECORDS: self.preprocess_records(records)}
        return data

    def grade(self, records, endpoint=GRADING_ENDPOINT):
        records = self.preprocess_records(records)
        return self.post(endpoint, data={RECORDS: records})
